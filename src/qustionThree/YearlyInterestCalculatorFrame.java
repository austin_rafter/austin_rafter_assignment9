package qustionThree;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DecimalFormat;

/*
*Austin Rafter
* CS 049J
*
* take initial balance, interest rate, and years
* loop through the years multiplying balance *((interest rate) / 100)
* add the interest after the equation to the balance and print the new balance
 */

public class YearlyInterestCalculatorFrame extends JFrame {

    private static final int FRAME_WIDTH = 500;
    private static final int FRAME_HEIGHT = 300;

    private static final double DEFAULT_INITIAL_BALANCE = 1000.00;

    private static final double DEFAULT_RATE_PERCENTAGE = 5.0;

    private static final int DEFAULT_YEARS = 5;

    private JLabel balanceLabel;

    private JTextField balanceText;
    private JLabel rateLabel;

    private JTextField rateText;

    private JLabel yearsLabel;

    private JTextField yearsText;

    private JButton interestButton;

    private JTextArea resultsArea;



    //constructor creates textArea for output and makes it uneditable
    //initiates textfields and button
    //adds everything to panel and sets frame width and height
    public YearlyInterestCalculatorFrame(){
        resultsArea = new JTextArea(10,30);
        resultsArea.setEditable(false);
        createTextField();
        createButton();
        createPanel();
        setSize(FRAME_WIDTH, FRAME_HEIGHT);
    }


    //create fields for the labels and entries
    //set the size for user entry of balance, rate, and years
    private void createTextField(){
        balanceLabel = new JLabel("Initial balance: $");
        rateLabel = new JLabel("Interest rate percentage: ");
        yearsLabel = new JLabel("Full years: ");

        final int FIELD_WIDTH = 10;
        balanceText = new JTextField(FIELD_WIDTH);
        balanceText.setText("" + DEFAULT_INITIAL_BALANCE);
        rateText = new JTextField(FIELD_WIDTH);
        rateText.setText("" + DEFAULT_RATE_PERCENTAGE);
        yearsText = new JTextField(FIELD_WIDTH);
        yearsText.setText("" + DEFAULT_YEARS);

    }


    /*
    add labels and text sections to a panel
    add the button the the panel
    ensure the results can scroll by adding it to
    a JScrollPane
    add panel to the frame
     */
    private void createPanel(){
        JPanel panel = new JPanel();
        panel.add(balanceLabel);
        panel.add(balanceText);
        panel.add(rateLabel);
        panel.add(rateText);
        panel.add(yearsLabel);
        panel.add(yearsText);
        panel.add(interestButton);

        JScrollPane scrollPane = new JScrollPane(resultsArea);
        panel.add(scrollPane);

        add(panel);

    }

         /*
          parse the rate and balance to doubles
          parse the year to ints
          loop through the years
          find the balance after the rate is applied
          */
    class AddYearlyInterestListener implements ActionListener {

        public void actionPerformed(ActionEvent event){

            double dAccountBalance = Double.parseDouble(balanceText.getText());
            double dInterestRate = Double.parseDouble(rateText.getText());
            int nYearsOfInterest = Integer.parseInt(yearsText.getText());
            double dSimpleInterestHold = dAccountBalance
            DecimalFormat df = new DecimalFormat("###.00");

            for(int i = 1; i <= nYearsOfInterest; i++){
                double dInterestToAdd = dSimpleInterestHold * (dInterestRate / 100.00);
                dAccountBalance += dInterestToAdd;
                resultsArea.append("After year: " + i + " Balance: $" + df.format(dAccountBalance) + "\n");
            }

        }
    }

    //create the button to attach the yearly
    //interest action listener
    private void createButton(){
        interestButton = new JButton("Calculate");
        ActionListener listener = new AddYearlyInterestListener();
        interestButton.addActionListener(listener);
    }
}


