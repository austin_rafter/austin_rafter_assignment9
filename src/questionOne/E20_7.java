package questionOne;

import javax.swing.*;
/*
 *Austin Rafter
 * CS 049J
 *
 *show frame for the random rectangles
 * menu
 */

public class E20_7 {
    public static void main(String[] args) {
        JFrame frame = new RectangleFrame();
        frame.setTitle("Random Rectangles menu");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);
    }
}
