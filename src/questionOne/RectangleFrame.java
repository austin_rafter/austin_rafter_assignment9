package questionOne;

import questionOne.RectangleComponent;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
/*
 *Austin Rafter
 * CS 049J
 *
 *create frame for the rectangles to show in
 * when user clicks more: if there are no rectangles add a single rectangle
 * then double on every more click
 *
 * when user clicks fewer cut the number of rectangles on screen in half
 * until one rectangle is less than remove it from the frame so there are no rectangles
 * if there are no rectangles and use clicks fewer
 * print to system that there are none left to remove
 */


public class RectangleFrame extends JFrame {
    public ArrayList<RectangleComponent> rectanglesOnFrame = new ArrayList<>();
    Container contentPane = getContentPane();

    //create Exit listener for user to exit through file menu
    static class ExitItemListener implements ActionListener {
        public void actionPerformed(ActionEvent event) {
            System.exit(0);
        }
    }

    //create the file menu with exit option
    public JMenu createFileMenu() {
        JMenu fileMenu = new JMenu("File");
        JMenuItem exitItem = new JMenuItem("Exit");
        ActionListener exitListener = new ExitItemListener();
        exitItem.addActionListener(exitListener);
        fileMenu.add(exitItem);
        return fileMenu;
    }

    //create the rectangle menu with fewer and more options
    public JMenu createRectangleMenu() {
        JMenu rectangleMenu = new JMenu("Add or delete rectangles");
        rectangleMenu.add(addRectangles("More"));
        rectangleMenu.add(removeRectangles("Fewer"));
        return rectangleMenu;
    }

    //more option will add rectangle components to an arraylist to store them
    //if arraylist has zero rectangle components add 1 and add the rectangle to
    // the frame
    // then double every time more is pressed and add those rectangles to the frame
    public JMenuItem addRectangles(String name) {
        class AddRectanglesListener implements ActionListener {
            public void actionPerformed(ActionEvent event) {
                if (rectanglesOnFrame.size() == 0) {
                    RectangleComponent rectangle = new RectangleComponent();
                    rectanglesOnFrame.add(rectangle);
                    add(rectangle);
                    repaint();
                    revalidate();
                } else {
                    int nArrayListSize = rectanglesOnFrame.size();
                    for (int i = 0; i < nArrayListSize; i++) {
                        RectangleComponent rectangle = new RectangleComponent();
                        rectanglesOnFrame.add(rectangle);
                        add(rectangle);
                        repaint();
                        revalidate();
                    }
                }
            }
        }
        JMenuItem item = new JMenuItem(name);
        ActionListener listener = new AddRectanglesListener();
        item.addActionListener(listener);
        return item;
    }

    //more option will remove half the  rectangle components from the arraylist
    //if arraylist has only 1 item it deletes it
    //if arraylist has zero items print out that there is nothing to delete
    //whenever fewer is pressed
    public JMenuItem removeRectangles(String name) {
        class RemoveRectanglesListener implements ActionListener {
            public void actionPerformed(ActionEvent event) {
                if (rectanglesOnFrame.size() == 1) {
                    remove(rectanglesOnFrame.get(0));
                    rectanglesOnFrame.remove(0);
                    repaint();
                    revalidate();
                } else if(rectanglesOnFrame.size() == 0) {
                    System.out.println("Nothing to remove");
                } else {
                        for (int i = 1; i <= ((rectanglesOnFrame.size())); i++) {
                            System.out.println(rectanglesOnFrame.size());
                            remove(rectanglesOnFrame.get(i/2));
                            rectanglesOnFrame.remove(i/2);
                            repaint();
                            revalidate();
                        }
                    }
                }
            }
        JMenuItem item = new JMenuItem(name);
        ActionListener listener = new RemoveRectanglesListener();
        item.addActionListener(listener);
        return item;
    }

    public RectangleFrame() {
        JMenuBar menuBar = new JMenuBar();
        setJMenuBar(menuBar);
        menuBar.add(createFileMenu());
        menuBar.add(createRectangleMenu());
        setSize(300, 400);

    }
}
