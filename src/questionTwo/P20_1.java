package questionTwo;
import javax.swing.*;
/*
 *Austin Rafter
 * CS 049J
 *
 *show frame for rectangle frame slider
 */

public class P20_1 {
    public static void main(String[] args) {
        JFrame frame = new RectangleTwoFrame();
        frame.setTitle("Random Rectangles menu");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);
    }

}
