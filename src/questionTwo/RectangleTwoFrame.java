package questionTwo;
import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.*;
import java.util.ArrayList;
/*
 *Austin Rafter
 * CS 049J
 *
 * create two panels, one to hold the slider the other to hold the rectangles
 * when user slides the slider right increase rectangles
 * when user slides the slider left decrease rectangles
 */

public class RectangleTwoFrame extends JFrame {
    public ArrayList<RectangleComponentTwo> rectanglesOnFrame = new ArrayList<>();
    private final JSlider rectangleSlider;
    JPanel jPanelOne = new JPanel();
    JPanel jPanelTwo = new JPanel();



    class RectangleListener implements ChangeListener {
        public void stateChanged(ChangeEvent event){
            setRectangleAmount();
        }
    }

    public void setRectangleAmount(){
        int nNumberOfRectangles = rectangleSlider.getValue();
        if(nNumberOfRectangles >= rectanglesOnFrame.size()){
            for(int i = 0; i < (nNumberOfRectangles - rectanglesOnFrame.size()); i++){
                RectangleComponentTwo rectangle = new RectangleComponentTwo();
                rectanglesOnFrame.add(rectangle);
               add(rectangle);
                repaint();
                revalidate();
            }
        }else if(nNumberOfRectangles < rectanglesOnFrame.size()){
            for(int i = 0; i <=(rectanglesOnFrame.size() - nNumberOfRectangles);i++ ){
                remove(rectanglesOnFrame.get(i));
                rectanglesOnFrame.remove(i);
                repaint();
                revalidate();
            }
        } else if( rectanglesOnFrame.size() == 0){
            System.out.println("Nothing to remove");
        }
    }



    public RectangleTwoFrame() {
        ChangeListener listener = new RectangleListener();

        rectangleSlider = new JSlider(0,255,1);
        rectangleSlider.setMinorTickSpacing(5);
        rectangleSlider.setMajorTickSpacing(10);
        rectangleSlider.setPaintTicks(true);
        rectangleSlider.addChangeListener(listener);
        jPanelTwo.add(rectangleSlider);
        add(jPanelTwo, BorderLayout.SOUTH);
        setSize(300, 400);

    }
}
