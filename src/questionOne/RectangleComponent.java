package questionOne;


import javax.swing.*;
import java.awt.*;
/*
 *Austin Rafter
 * CS 049J
 *
 *create rectangle object at a random location
 * relative to the width and height of the frame
 * minus the width and height of the rectangle
 * respectively
 */


public class RectangleComponent extends JComponent {

    public void paintComponent(Graphics g){
        Graphics2D g2 = (Graphics2D) g;
        Rectangle randomRectangle = new Rectangle((int)(Math.random() * (getWidth() - 30)), (int)(Math.random() * (getHeight() - 20)), 30, 20);
        g2.draw(randomRectangle);
    }
}
