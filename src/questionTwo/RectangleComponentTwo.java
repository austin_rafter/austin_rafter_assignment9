package questionTwo;

import javax.swing.*;
import java.awt.*;
/*
 *Austin Rafter
 * CS 049J
 *
 * create rectangle at random location on the frame
 */

public class RectangleComponentTwo extends JComponent{
    public void paintComponent(Graphics g){
        Graphics2D g2 = (Graphics2D) g;
        Rectangle randomRectangle = new Rectangle((int)(Math.random() * (getWidth() - 30)), (int)(Math.random() * (getHeight() - 20)), 30, 20);
        g2.draw(randomRectangle);
    }
}
