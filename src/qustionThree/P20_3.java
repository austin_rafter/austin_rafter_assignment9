package qustionThree;

import javax.swing.*;
/*
 *Austin Rafter
 * CS 049J
 *
 *show the frame for user to enter the information to find
 * the yearly interest
 */

public class P20_3 {
    public static void main(String[] args) {
        JFrame frame = new YearlyInterestCalculatorFrame();
        frame.setTitle("Random Rectangles menu");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);
    }
}
